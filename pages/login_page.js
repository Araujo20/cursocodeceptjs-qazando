const { I } = inject();

module.exports = {

  fields:{
    email:'~email',
    password:'~senha'
  },

  buttons:{
    enter:'~entrar',
  },

  messages:{
    login_error:'~lognFail',
  },


  doLogin(email, password){
    I.waitForElement(this.fields.email, 5)
    I.fillField(this.fields.email,email)
    I.fillField(this.fields.password,password)
    I.tap(this.buttons.enter)
  },

  checkLoginError(){
    // check
    I.waitForElement(this.messages.login_error, 5)
    I.seeElement(this.messages.login_error)
   }

}
