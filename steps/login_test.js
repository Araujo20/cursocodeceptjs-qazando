Feature('login');

const {I ,login_page, home_page } = inject()

Before(() =>{

});

Scenario('Login with success.', ({ I }) => {
    login_page.doLogin('teste@teste.com', '123456')
    home_page.checkLoginSucess()

});  

    Scenario('Login with error.', ({ I }) => {
        login_page.doLogin('testes@teste.com', '123456')
        login_page.checkLoginError()
 
});
