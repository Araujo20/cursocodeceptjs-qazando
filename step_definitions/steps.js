const { I } = inject();
// Add in your custom step files

Given('I have a defined step', () => {
  // TODO: replace with your own step
});

Given(/^I fill email$/, () => {
	I.waitForElement(this.fields.email, 5)
    I.fillField(this.fields.email,email)
});

(/^I fill password$/, () => {
  I.fillField(this.fields.password,password)
});

When(/^I tap on Entrar$/, () => {
	I.tap(this.buttons.enter)
});

Then(/^I see the Salvar button$/, () => {
	return true;
});

